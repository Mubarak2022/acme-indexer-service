package com.acme.indexerservice.model;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestResponse {
    private Integer statusCode;
    private String statusMessage;
}
