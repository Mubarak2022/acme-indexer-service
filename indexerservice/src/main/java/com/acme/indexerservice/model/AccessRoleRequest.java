package com.acme.indexerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccessRoleRequest {
    @NotNull
    private Integer userRef;
    private Integer docRef;
    private Integer folderRef;

}
