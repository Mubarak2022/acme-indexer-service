package com.acme.indexerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ViewDTO {
    private Integer id;
    private String name;
    private Boolean isFolder = false;
    private Integer creatorId;
}
