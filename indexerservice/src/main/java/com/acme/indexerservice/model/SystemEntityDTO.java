package com.acme.indexerservice.model;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SystemEntityDTO {
    private Integer id;
    private String name;
    private Integer parentFolderId;
}
