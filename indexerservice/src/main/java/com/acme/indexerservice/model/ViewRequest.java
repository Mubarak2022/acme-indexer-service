package com.acme.indexerservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ViewRequest {
    @NotNull
    private Integer userId;
    private Integer folderRef;
    private String folderName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate startDateFilter;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate endDateFilter;
    private Integer creatorFilter;
    private String searchText;
}
