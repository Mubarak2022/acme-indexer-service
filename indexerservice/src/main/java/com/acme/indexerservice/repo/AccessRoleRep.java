package com.acme.indexerservice.repo;

import com.acme.indexerservice.entity.AccessRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface AccessRoleRep extends JpaRepository<AccessRole, Integer> {

    List<AccessRole> findByUserId(Integer userId);

    @Transactional
    @Modifying
    @Query("delete from AccessRole u where u.userId = ?1 and u.docId = ?2 and u.folderId = ?3")
    int deleteAccessRole(String userId, Integer docId, Integer folderId);


}
