package com.acme.indexerservice.repo;

import com.acme.indexerservice.entity.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface FolderRep extends JpaRepository<Folder, Integer> {

    public Folder findByFolderId(Integer folderId);


    public List<Folder> findByParentFolder(Folder parentFolderId);

    Folder findByFolderName(String folderName);

    @Transactional
    @Modifying
    @Query(value = "delete from Folder u where u.folderId = ?1 AND u.is_Deleted = true AND u.parent_Folder = ?2", nativeQuery = true)
    int deleteByFolderId(Integer folderId);

    @Query(value = "select * from Folder u where u.folder_name = ?1 AND u.parent_folder =?2", nativeQuery = true)
    Folder getFolderData(String folderName, Integer parentFolder);

    @Query(value = "select * from Folder u where u.folder_name = ?1 AND u.parent_folder is null", nativeQuery = true)
    Folder getRootFolderData(String folderName);

}
