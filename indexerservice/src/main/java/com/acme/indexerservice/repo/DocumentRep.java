package com.acme.indexerservice.repo;

import com.acme.indexerservice.entity.Document;
import com.acme.indexerservice.entity.Folder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DocumentRep extends JpaRepository<Document, Integer> {

    List<Document> findByParentFolder(Folder parentFolderId);

    Document findByDocName(String filename);

    @Transactional
    @Modifying
    @Query(value = "delete from Document u where u.doc_name = ?1 AND u.is_Deleted = true AND u.parent_Folder = ?2", nativeQuery = true)
    int deleteByDocIdAndParentFolder(String docName, Integer parentFolder);

    @Query(value = "select * from document u where u.doc_name = ?1 AND u.parent_folder =?2", nativeQuery = true)
    Document getDocumentData(String docName, Integer parentFolder);


}
