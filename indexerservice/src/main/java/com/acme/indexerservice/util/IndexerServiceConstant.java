package com.acme.indexerservice.util;

import java.io.Serializable;

public final class IndexerServiceConstant implements Serializable {
    public static final String INDEXER = "/indexer";
    public static final String ACME = "/acme";
    public static final String ROLE = "/role";
    public static final String AUTHORIZATION = "Authorization";
    public static final String MTOKEN = "MToken ";
    public static final String AUTH_WHITELIST_VALUE1 = "/acme/indexer/**";
    public static final String AUTH_WHITELIST_VALUE2 = "/v3/api-docs/**";
    public static final String AUTH_WHITELIST_VALUE3 = "/swagger-ui/**";
    public static final String AUTH_WHITELIST_VALUE4 = "/acme/view";
    public static final String AUTH_WHITELIST_VALUE5 = "/acme/role";
    public static final String RESOURCE_NOT_FOUND_EXCEPTION = "ResourceNotFoundException : {}";
    public static final String CUSTOM_EXCEPTION = "CustomException : {}";
    public static final String NO_DATA_FOUND_EXCEPTION = "NoDataFoundException : {}";
    public static final String GENERIC_EXCEPTION = "GenericException : {}";

    public static final String ADMIN = "ADMIN";
    public static final String FILE_PATH_SHOULD_BE_INCLUDED = "file path Should be included";
    public static final String FILE_PERMANENTLY_DELETED_SUCCESSFULLY = "File Permanently Deleted Successfully";
    public static final String FILE_MOVED_TO_BIN_SUCCESSFULLY = "File Moved to Bin Successfully";
}
