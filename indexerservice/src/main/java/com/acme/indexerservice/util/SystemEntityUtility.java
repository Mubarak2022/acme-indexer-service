package com.acme.indexerservice.util;

import com.acme.indexerservice.entity.Document;
import com.acme.indexerservice.entity.Folder;
import com.acme.indexerservice.exception.CustomException;
import com.acme.indexerservice.model.SystemEntityDTO;
import com.acme.indexerservice.repo.DocumentRep;
import com.acme.indexerservice.repo.FolderRep;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SystemEntityUtility {
    private final FolderRep folderRep;
    private final DocumentRep documentRep;

    public SystemEntityUtility(FolderRep folderRep, DocumentRep documentRep) {
        this.folderRep = folderRep;
        this.documentRep = documentRep;
    }

    private static void nullCheck(Folder folder) {
        if (folder == null) {
            throw new CustomException("Record Not found");
        }
    }

    //getting the Parent folder id and id from file path
    public SystemEntityDTO splitParentFolderIdAndFileId(String filePath) {
        log.info("filePath :{}", filePath);
        SystemEntityDTO systemEntityDTO = new SystemEntityDTO();

        String[] parentNames = filePath.split("/");
        Folder parentFolder = null;
        int count = 0;
        count = parentNames.length;
        log.info("length :{}", count);
        if (count == 1) {
            systemEntityDTO.setName(filePath);
            return systemEntityDTO;
        }

        for (int i = 0; i < count - 1; ++i) {
            if (i == 0) {
                parentFolder = folderRep.getRootFolderData(parentNames[i]);
            } else {
                nullCheck(parentFolder);
                parentFolder = folderRep.getFolderData(parentNames[i], parentFolder.getFolderId());
            }
        }
        log.info("parentFolder :{}", parentFolder);

        if (parentNames[parentNames.length - 1].lastIndexOf(".") >= 0) {
            nullCheck(parentFolder);
            Document document = documentRep.getDocumentData(parentNames[parentNames.length - 1], parentFolder.getFolderId());
            if (document != null)
                systemEntityDTO.setId(document.getDocId());
        }
        systemEntityDTO.setName(parentNames[parentNames.length - 1]);
        if (parentFolder != null)
            systemEntityDTO.setParentFolderId(parentFolder.getFolderId());

        log.info("systemEntityDTO :{}", systemEntityDTO);
        return systemEntityDTO;
    }
}