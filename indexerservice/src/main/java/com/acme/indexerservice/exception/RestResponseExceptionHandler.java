package com.acme.indexerservice.exception;

import com.acme.indexerservice.model.RestResponse;
import com.acme.indexerservice.util.IndexerServiceConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RestResponseExceptionHandler {


    @ExceptionHandler
    public ResponseEntity<RestResponse> handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
        log.info(IndexerServiceConstant.RESOURCE_NOT_FOUND_EXCEPTION, resourceNotFoundException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_FOUND.value(), resourceNotFoundException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleCustomException(CustomException customException) {
        log.info(IndexerServiceConstant.CUSTOM_EXCEPTION, customException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_ACCEPTABLE.value(), customException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleNoDataFoundException(NoDataFoundException noDataFoundException) {
        log.info(IndexerServiceConstant.NO_DATA_FOUND_EXCEPTION, noDataFoundException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.NOT_FOUND.value(), noDataFoundException.getLocalizedMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestResponse> handleDataIntegrityViolationException(DataIntegrityViolationException dataIntegrityViolationException) {
        log.info("DataIntegrityViolationException :{}", dataIntegrityViolationException);
        return new ResponseEntity<>(new RestResponse(HttpStatus.FORBIDDEN.value(), "This action is not allowed"), new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<RestResponse> handleGenericException(Exception exception) {
        log.info(IndexerServiceConstant.GENERIC_EXCEPTION, exception);
        return new ResponseEntity<>(new RestResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), exception.getLocalizedMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
