package com.acme.indexerservice.service;

import com.acme.indexerservice.entity.AccessRole;
import com.acme.indexerservice.exception.CustomException;
import com.acme.indexerservice.model.UserDTO;
import com.acme.indexerservice.repo.AccessRoleRep;
import com.acme.indexerservice.util.IndexerServiceConstant;
import com.acme.indexerservice.util.JWTUtility;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class UserPermissionValidator {
    private final JWTUtility jwtUtility;
    private final AccessRoleRep accessRoleRep;


    public UserPermissionValidator(JWTUtility jwtUtility, AccessRoleRep accessRoleRep) {
        this.jwtUtility = jwtUtility;
        this.accessRoleRep = accessRoleRep;

    }

    public Integer getUserId(HttpServletRequest request) {
        UserDTO userDTO = jwtUtility.getUserDataFromToken(request);
        return userDTO.getUserId();
    }

    public Integer checkUserValidation(HttpServletRequest request, Integer id, boolean isDocument) {
        UserDTO userDTO = jwtUtility.getUserDataFromToken(request);
        Integer userId = userDTO.getUserId();

        if (userDTO.getRole().equalsIgnoreCase(IndexerServiceConstant.ADMIN)) {
            return userId;
        } else {
            List<AccessRole> accessRoles = accessRoleRep.findByUserId(userId);

            if (isDocument) {
                AccessRole accessRole = accessRoles.stream()
                        .filter(aRole -> id.equals(aRole.getDocId()))
                        .findAny()
                        .orElse(null);
                if (accessRole != null) {
                    return userId;
                } else {
                    throw new CustomException("Current user do not have permission");
                }

            } else {
                AccessRole accessRole = accessRoles.stream()
                        .filter(aRole -> id.equals(aRole.getFolderId()))
                        .findAny()
                        .orElse(null);
                if (accessRole != null) {
                    return userId;
                } else {
                    throw new CustomException("Current user do not have permission");
                }
            }

        }

    }
}
