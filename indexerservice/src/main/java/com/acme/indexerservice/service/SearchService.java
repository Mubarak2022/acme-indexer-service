package com.acme.indexerservice.service;

import com.acme.indexerservice.model.ViewRequest;
import com.acme.indexerservice.model.ViewResponse;

public interface SearchService {
    public ViewResponse getView(ViewRequest viewRequest);

}
