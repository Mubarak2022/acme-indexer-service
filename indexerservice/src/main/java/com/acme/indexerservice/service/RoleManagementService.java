package com.acme.indexerservice.service;

import com.acme.indexerservice.model.AccessRoleRequest;
import com.acme.indexerservice.model.RestResponse;

public interface RoleManagementService {

    String getRole(Integer userId);

    RestResponse addRole(AccessRoleRequest accessRoleRequest);

    RestResponse deleteRole(AccessRoleRequest accessRoleRequest);
}
