package com.acme.indexerservice.service;

import com.acme.indexerservice.entity.AccessRole;
import com.acme.indexerservice.entity.Document;
import com.acme.indexerservice.entity.Folder;
import com.acme.indexerservice.model.ViewDTO;
import com.acme.indexerservice.model.ViewRequest;
import com.acme.indexerservice.model.ViewResponse;
import com.acme.indexerservice.repo.AccessRoleRep;
import com.acme.indexerservice.repo.DocumentRep;
import com.acme.indexerservice.repo.FolderRep;
import com.acme.indexerservice.repo.UserRep;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class SearchServiceImpl implements SearchService {
    private final DocumentRep documentRep;
    private final FolderRep folderRep;
    private final AccessRoleRep accessRoleRep;
    private final UserRep userRep;

    public SearchServiceImpl(DocumentRep documentRep, FolderRep folderRep, AccessRoleRep accessRoleRep, UserRep userRep) {
        this.documentRep = documentRep;
        this.folderRep = folderRep;
        this.accessRoleRep = accessRoleRep;
        this.userRep = userRep;
    }

    private static void filterDocument(ViewRequest viewRequest, List<ViewDTO> viewDTOList, Set<Integer> accessRolesDocumentList, Document document, String userRole) {
        if (((userRole != null && userRole.equalsIgnoreCase("ADMIN")) || accessRolesDocumentList.contains(document.getDocId()))
                && (viewRequest.getCreatorFilter() == null || document.getCreatedByUser().equals(viewRequest.getCreatorFilter()))
                && (viewRequest.getSearchText() == null || document.getDocName().contains(viewRequest.getSearchText()))
                && (viewRequest.getStartDateFilter() == null || document.getCreatedDate().toLocalDate().isAfter(viewRequest.getStartDateFilter()))
                && (viewRequest.getEndDateFilter() == null || viewRequest.getEndDateFilter().isAfter(document.getCreatedDate().toLocalDate()))

        ) {
            ViewDTO viewDTO = new ViewDTO();
            viewDTO.setId(document.getDocId());
            viewDTO.setName(document.getDocName());
            viewDTO.setCreatorId(document.getCreatedByUser());
            viewDTOList.add(viewDTO);
        }
    }

    private static void filterFolder(ViewRequest viewRequest, List<ViewDTO> viewDTOList, Set<Integer> accessRolesFolderList, Folder folder, String userRole) {
        if (((userRole != null && userRole.equalsIgnoreCase("ADMIN")) || accessRolesFolderList.contains(folder.getFolderId()))
                && (viewRequest.getCreatorFilter() == null || folder.getCreatedByUser().equals(viewRequest.getCreatorFilter()))
                && (viewRequest.getSearchText() == null || folder.getFolderName().contains(viewRequest.getSearchText()))
                && (viewRequest.getStartDateFilter() == null || folder.getCreatedDate().toLocalDate().isAfter(viewRequest.getStartDateFilter()))
                && (viewRequest.getEndDateFilter() == null || viewRequest.getEndDateFilter().isAfter(folder.getCreatedDate().toLocalDate()))


        ) {
            ViewDTO viewDTO = new ViewDTO();
            viewDTO.setId(folder.getFolderId());
            viewDTO.setName(folder.getFolderName());
            viewDTO.setCreatorId(folder.getCreatedByUser());
            viewDTO.setIsFolder(true);
            viewDTOList.add(viewDTO);
        }
    }

    @Override
    public ViewResponse getView(ViewRequest viewRequest) {
        List<ViewDTO> viewDTOList = new ArrayList<>();
        // Fetching file and folder list
        List<Document> documentList = documentRep.findByParentFolder(folderRep.findByFolderId(viewRequest.getFolderRef()));
        List<Folder> folderList = folderRep.findByParentFolder(folderRep.findByFolderId(viewRequest.getFolderRef()));
        List<AccessRole> accessRoleList = accessRoleRep.findByUserId(viewRequest.getUserId());
        //check for the User Role
        String userRole = userRep.findById(viewRequest.getUserId()).getRole();

        Set<Integer> accessRolesFolderList = new HashSet<>();
        Set<Integer> accessRolesDocumentList = new HashSet<>();
        for (AccessRole accessRole : accessRoleList) {
            accessRolesFolderList.add(accessRole.getFolderId());
            accessRolesDocumentList.add(accessRole.getDocId());
        }
        log.info("accessRolesFolderList " + accessRolesFolderList);
        log.info("folderList " + folderList);
        log.info("accessRolesDocumentList " + accessRolesDocumentList);
        log.info("documentList " + documentList);
        log.info("viewRequest " + viewRequest);
        log.info("userRole " + userRole);

        for (Folder folder : folderList) {
            /* Filter by AccessRole */
            filterFolder(viewRequest, viewDTOList, accessRolesFolderList, folder, userRole);

        }

        for (Document document : documentList) {
            /* Filter by AccessRole */
            filterDocument(viewRequest, viewDTOList, accessRolesDocumentList, document, userRole);
        }

        /* Sorting by Name, Folder and Document */
        viewDTOList.sort(Comparator.comparing(ViewDTO::getName));
        viewDTOList.sort(Comparator.comparing(ViewDTO::getIsFolder));

        return new ViewResponse(viewDTOList);
    }
}
