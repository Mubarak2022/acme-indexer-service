package com.acme.indexerservice.service;

import com.acme.indexerservice.entity.AccessRole;
import com.acme.indexerservice.model.AccessRoleRequest;
import com.acme.indexerservice.model.RestResponse;
import com.acme.indexerservice.repo.AccessRoleRep;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class RoleManagementServiceImpl implements RoleManagementService {
    private final AccessRoleRep accessRoleRep;

    public RoleManagementServiceImpl(AccessRoleRep accessRoleRep) {
        this.accessRoleRep = accessRoleRep;
    }

    @Override
    public String getRole(Integer userId) {
        return accessRoleRep.findByUserId(userId).toString();
    }

    @Override
    public RestResponse addRole(AccessRoleRequest accessRoleRequest) {
        AccessRole accessRole = new AccessRole();
        accessRoleRep.save(copyAccessRoleRequestBeans(accessRoleRequest, accessRole));
        return new RestResponse(HttpStatus.OK.value(), "Added Successfully");
    }

    @Override
    public RestResponse deleteRole(AccessRoleRequest accessRoleRequest) {
        AccessRole accessRole = new AccessRole();
        accessRoleRep.delete(copyAccessRoleRequestBeans(accessRoleRequest, accessRole));
        return new RestResponse(HttpStatus.OK.value(), "Deleted Successfully");
    }

    private AccessRole copyAccessRoleRequestBeans(AccessRoleRequest accessRoleRequest, AccessRole accessRole) {

        accessRole.setUserId(accessRoleRequest.getUserRef());
        accessRole.setDocId(accessRoleRequest.getDocRef() != null ? accessRoleRequest.getDocRef() : accessRole.getDocId());
        accessRole.setFolderId(accessRoleRequest.getFolderRef() != null ? accessRoleRequest.getFolderRef() : accessRole.getFolderId());

        return accessRole;
    }


}
