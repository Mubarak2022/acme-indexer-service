package com.acme.indexerservice.service;

import com.acme.indexerservice.entity.AccessRole;
import com.acme.indexerservice.entity.Document;
import com.acme.indexerservice.entity.Folder;
import com.acme.indexerservice.exception.CustomException;
import com.acme.indexerservice.model.IndexerRequest;
import com.acme.indexerservice.model.RestResponse;
import com.acme.indexerservice.model.SystemEntityDTO;
import com.acme.indexerservice.repo.AccessRoleRep;
import com.acme.indexerservice.repo.DocumentRep;
import com.acme.indexerservice.repo.FolderRep;
import com.acme.indexerservice.util.SystemEntityUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;

import static com.acme.indexerservice.util.IndexerServiceConstant.FILE_MOVED_TO_BIN_SUCCESSFULLY;
import static com.acme.indexerservice.util.IndexerServiceConstant.FILE_PERMANENTLY_DELETED_SUCCESSFULLY;

@Service
@Slf4j
public class IndexerServiceImpl implements IndexerService {
    private final DocumentRep documentRep;
    private final FolderRep folderRep;
    private final AccessRoleRep accessRoleRep;
    private final SystemEntityUtility systemEntityUtility;
    private final UserPermissionValidator userPermissionValidator;

    public IndexerServiceImpl(DocumentRep documentRep, FolderRep folderRep, AccessRoleRep accessRoleRep, SystemEntityUtility systemEntityUtility, UserPermissionValidator userPermissionValidator) {
        this.documentRep = documentRep;
        this.folderRep = folderRep;
        this.accessRoleRep = accessRoleRep;
        this.systemEntityUtility = systemEntityUtility;
        this.userPermissionValidator = userPermissionValidator;

    }


    @Override
    @Transactional
    public RestResponse addFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request) {
        Integer entityId;
        Integer userId = userPermissionValidator.getUserId(request);

        AccessRole accessRole = new AccessRole();
        if (indexerRequest.isDocument()) {
            Document document = new Document();
            copyDocumentRequestBeans(indexerRequest.getFilePath(), document);
            document.setCreatedDate(ZonedDateTime.now());
            document.setCreatedByUser(userId);
            entityId = documentRep.save(document).getDocId();
            //set file access
            accessRole.setDocId(entityId);
            accessRole.setFolderId(0);

        } else {
            Folder folder = new Folder();
            copyDocumentRequestBeans(indexerRequest.getFilePath(), folder);
            folder.setCreatedDate(ZonedDateTime.now());
            folder.setCreatedByUser(userId);
            entityId = folderRep.save(folder).getFolderId();
            //set folder access
            accessRole.setFolderId(entityId);
            accessRole.setDocId(0);
        }

        //Save access details

        accessRole.setUserId(userId);
        accessRoleRep.save(accessRole);
        return new RestResponse(HttpStatus.OK.value(), entityId.toString());
    }

    @Override
    @Transactional
    public RestResponse updateFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request) {

        if (indexerRequest.getNewParentFolderId() != null) {
            modifyFileSystemEntity(indexerRequest, request);
        } else {
            updateFilesFolders(indexerRequest, request);
        }

        return new RestResponse(HttpStatus.OK.value(), "Updated Successfully");
    }


    private void updateFilesFolders(IndexerRequest indexerRequest, HttpServletRequest request) {
        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(indexerRequest.getFilePath());
        if (systemEntityDTO != null) {
            if (indexerRequest.isDocument()) {
                Document document = documentRep.getDocumentData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
                copyDocumentRequestBeans(indexerRequest.getFilePath(), document);
                document.setModifiedDate(ZonedDateTime.now());
                documentRep.save(document);
            } else {
                Folder folder = folderRep.getFolderData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
                copyDocumentRequestBeans(indexerRequest.getFilePath(), folder);
                folder.setCreatedDate(ZonedDateTime.now());
                folder.setCreatedByUser(userPermissionValidator.getUserId(request));
                folderRep.save(folder);
            }
        }
    }

    @Override
    @Transactional
    public RestResponse deleteFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request) {


        if (indexerRequest.isPermanentDeleteFlag()) {
            return permanentDelete(indexerRequest, request) > 0 ?
                    new RestResponse(HttpStatus.OK.value(), FILE_PERMANENTLY_DELETED_SUCCESSFULLY)
                    : new RestResponse(HttpStatus.FORBIDDEN.value(), "File is not sent to Bin");
        } else {
            moveToBin(indexerRequest, request);
            return new RestResponse(HttpStatus.OK.value(), FILE_MOVED_TO_BIN_SUCCESSFULLY);
        }

    }

    private void moveToBin(IndexerRequest indexerRequest, HttpServletRequest request) {
        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(indexerRequest.getFilePath());
        if (systemEntityDTO != null) {
            if (indexerRequest.isDocument()) {
                Document document = documentRep.getDocumentData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
                copyDocumentRequestBeans(indexerRequest.getFilePath(), document);
                document.setIsDeleted(true);
                document.setDeletedDate(ZonedDateTime.now());
                document.setDeletedByUser(userPermissionValidator.getUserId(request));
                documentRep.save(document);
            } else {
                Folder folder = folderRep.getFolderData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
                copyDocumentRequestBeans(indexerRequest.getFilePath(), folder);
                folder.setIsDeleted(true);
                folder.setDeletedDate(ZonedDateTime.now());
                folder.setDeletedByUser(userPermissionValidator.getUserId(request));
                folderRep.save(folder);
            }
        }
    }

    private Integer permanentDelete(IndexerRequest indexerRequest, HttpServletRequest request) {
        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(indexerRequest.getFilePath());
        if (systemEntityDTO != null) {
            userPermissionValidator.checkUserValidation(request, systemEntityDTO.getId(), true);
            return documentRep.deleteByDocIdAndParentFolder(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
        }
        return 0;
    }


    private void modifyFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request) {
        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(indexerRequest.getFilePath());
        if (systemEntityDTO != null) {
            if (indexerRequest.isDocument()) {
                Document document = documentRep.getDocumentData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
                copyDocumentRequestBeans(indexerRequest.getFilePath(), document);
                /* Enabling the file copying */
                if (indexerRequest.isCopyFlag()) {
                    document.setDocId(null);
                    document.setCreatedDate(ZonedDateTime.now());
                    document.setCreatedByUser(userPermissionValidator.getUserId(request));
                } else {
                    document.setModifiedDate(ZonedDateTime.now());
                }
                document.setParentFolder(folderRep.findByFolderId(indexerRequest.getNewParentFolderId()));
                documentRep.save(document);
            } else {
                Folder folder = folderRep.getFolderData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
                copyDocumentRequestBeans(indexerRequest.getFilePath(), folder);
                /* Enabling the folder copying */
                if (indexerRequest.isCopyFlag()) {
                    folder.setFolderId(null);
                    folder.setCreatedDate(ZonedDateTime.now());
                    folder.setCreatedByUser(userPermissionValidator.getUserId(request));
                } else {
                    folder.setModifiedDate(ZonedDateTime.now());
                }
                folder.setParentFolder(folderRep.findByFolderId(indexerRequest.getNewParentFolderId()));
                folderRep.save(folder);
            }
        }
    }

    @Override
    public RestResponse getFile(String filePath, HttpServletRequest request) {

        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(filePath);

        Document document = documentRep.getDocumentData(systemEntityDTO.getName(), systemEntityDTO.getParentFolderId());
        if (document == null) {
            throw new CustomException("No Record Found ");
        }

        userPermissionValidator.checkUserValidation(request, document.getDocId(), true);
        return new RestResponse(HttpStatus.OK.value(), document.getDocId().toString());

    }


    private void copyDocumentRequestBeans(String filepath, Document document) {
        log.info("copyDocumentRequestBeans filepath :{}", filepath);
        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(filepath);
        if (systemEntityDTO != null) {
            document.setDocName(systemEntityDTO.getName() != null ? systemEntityDTO.getName() : document.getDocName());
            document.setParentFolder(systemEntityDTO.getParentFolderId() != null ? folderRep.findByFolderId(systemEntityDTO.getParentFolderId()) : document.getParentFolder());
            log.info("copyDocumentRequestBeans document :{}", document);
        }

    }

    private void copyDocumentRequestBeans(String filepath, Folder folder) {
        log.info("copyDocumentRequestBeans filepath :{}", filepath);
        SystemEntityDTO systemEntityDTO = systemEntityUtility.splitParentFolderIdAndFileId(filepath);
        if (systemEntityDTO != null) {
            folder.setFolderName(systemEntityDTO.getName() != null ? systemEntityDTO.getName() : folder.getFolderName());
            folder.setParentFolder(systemEntityDTO.getParentFolderId() != null ? folderRep.findByFolderId(systemEntityDTO.getParentFolderId()) : folder.getParentFolder());
            log.info("copyDocumentRequestBeans folder :{}", folder);
        }
    }

}
