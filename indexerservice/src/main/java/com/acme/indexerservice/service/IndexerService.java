package com.acme.indexerservice.service;

import com.acme.indexerservice.model.IndexerRequest;
import com.acme.indexerservice.model.RestResponse;

import javax.servlet.http.HttpServletRequest;

public interface IndexerService {
    RestResponse addFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request);

    RestResponse updateFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request);

    RestResponse deleteFileSystemEntity(IndexerRequest indexerRequest, HttpServletRequest request);

    RestResponse getFile(String filePath, HttpServletRequest request);

}
