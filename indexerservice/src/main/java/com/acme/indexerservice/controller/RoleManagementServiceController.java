package com.acme.indexerservice.controller;

import com.acme.indexerservice.model.AccessRoleRequest;
import com.acme.indexerservice.model.RestResponse;
import com.acme.indexerservice.service.RoleManagementService;
import com.acme.indexerservice.util.IndexerServiceConstant;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(IndexerServiceConstant.ACME)
public class RoleManagementServiceController {
    private final RoleManagementService roleManagementService;

    public RoleManagementServiceController(RoleManagementService roleManagementService) {
        this.roleManagementService = roleManagementService;
    }

    @GetMapping(IndexerServiceConstant.ROLE)
    public String getRole(@RequestParam("userEmail") Integer userId) {

        return roleManagementService.getRole(userId);
    }

    @PostMapping(IndexerServiceConstant.ROLE)
    public RestResponse addRole(@Validated @RequestBody AccessRoleRequest accessRoleRequest) {

        return roleManagementService.addRole(accessRoleRequest);
    }

    @DeleteMapping(IndexerServiceConstant.ROLE)
    public RestResponse deleteRole(@Validated @RequestBody AccessRoleRequest accessRoleRequest) {

        return roleManagementService.deleteRole(accessRoleRequest);
    }
}
