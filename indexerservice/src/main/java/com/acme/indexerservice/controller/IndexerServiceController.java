package com.acme.indexerservice.controller;

import com.acme.indexerservice.model.IndexerRequest;
import com.acme.indexerservice.model.RestResponse;
import com.acme.indexerservice.service.IndexerService;
import com.acme.indexerservice.util.IndexerServiceConstant;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(IndexerServiceConstant.ACME)
public class IndexerServiceController {
    private final IndexerService indexerService;

    public IndexerServiceController(IndexerService indexerService) {
        this.indexerService = indexerService;
    }

    @GetMapping(IndexerServiceConstant.INDEXER)
    public RestResponse getFileId(@RequestParam("filePath") String filePath, HttpServletRequest request) {

        return indexerService.getFile(filePath, request);
    }

    @PostMapping(IndexerServiceConstant.INDEXER)
    public RestResponse addFileSystemEntity(@Validated @RequestBody IndexerRequest indexerRequest, HttpServletRequest request) {

        return indexerService.addFileSystemEntity(indexerRequest, request);
    }

    @PutMapping(IndexerServiceConstant.INDEXER)
    public RestResponse updateFileSystemEntity(@Validated @RequestBody IndexerRequest indexerRequest, HttpServletRequest request) {

        return indexerService.updateFileSystemEntity(indexerRequest, request);
    }

    @DeleteMapping(IndexerServiceConstant.INDEXER)
    public RestResponse deleteFileSystemEntity(@Validated @RequestBody IndexerRequest indexerRequest, HttpServletRequest request) {

        return indexerService.deleteFileSystemEntity(indexerRequest, request);
    }

}
