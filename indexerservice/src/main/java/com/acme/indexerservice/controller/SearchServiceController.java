package com.acme.indexerservice.controller;

import com.acme.indexerservice.model.ViewRequest;
import com.acme.indexerservice.model.ViewResponse;
import com.acme.indexerservice.service.SearchService;
import com.acme.indexerservice.util.IndexerServiceConstant;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(IndexerServiceConstant.ACME)
public class SearchServiceController {
    private final SearchService searchService;

    public SearchServiceController(SearchService searchService) {
        this.searchService = searchService;
    }

    @PostMapping("/view")
    public ViewResponse getView(@Validated @RequestBody ViewRequest viewRequest) {

        return searchService.getView(viewRequest);
    }

}
