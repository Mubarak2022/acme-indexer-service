package com.acme.indexerservice.filter;


import com.acme.indexerservice.service.UserAuthenticationService;
import com.acme.indexerservice.util.IndexerServiceConstant;
import com.acme.indexerservice.util.JWTUtility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class JwtFilter extends OncePerRequestFilter {

    private final JWTUtility jwtUtility;

    private final UserAuthenticationService userAuthenticationService;

    public JwtFilter(JWTUtility jwtUtility, UserAuthenticationService userAuthenticationService) {
        this.jwtUtility = jwtUtility;
        this.userAuthenticationService = userAuthenticationService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String authorization = request.getHeader(IndexerServiceConstant.AUTHORIZATION);
        String token = null;
        String userId = null;
        String role;
        String tokenSubject;
        if (null != authorization && authorization.startsWith(IndexerServiceConstant.MTOKEN)) {
            token = authorization.substring(7);
            tokenSubject = jwtUtility.getUsernameFromToken(token);
            if (tokenSubject != null) {
                userId = tokenSubject.split("#")[0];
                role = tokenSubject.split("#")[1];
                log.info("Username :{}  Role :{}", userId, role);
            }
        }

        if (null != userId && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userAuthenticationService.loadUserByUserId(Integer.parseInt(userId));

            if (jwtUtility.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)
                );

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }

        }
        filterChain.doFilter(request, response);
    }
}
