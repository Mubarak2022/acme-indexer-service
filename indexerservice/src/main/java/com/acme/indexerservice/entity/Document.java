package com.acme.indexerservice.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "DOCUMENT", uniqueConstraints = {@UniqueConstraint(name = "uniqueFilenameAndParentId", columnNames = {"DOC_NAME", "PARENT_FOLDER"})})
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DOC_ID")
    private Integer docId;

    @Column(name = "DOC_NAME", length = 200)
    private String docName;

    @ManyToOne
    @JoinColumn(name = "PARENT_FOLDER")
    private Folder parentFolder;

    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

    @Column(name = "CREATED_DATE")
    private ZonedDateTime createdDate;

    @Column(name = "MODIFIED_DATE")
    private ZonedDateTime modifiedDate;

    @Column(name = "DELETED_DATE")
    private ZonedDateTime deletedDate;

    @NotNull
    @Column(name = "CREATED_BY_USER")
    private Integer createdByUser;

    @Column(name = "DELETED_BY_USER")
    private Integer deletedByUser;


}
