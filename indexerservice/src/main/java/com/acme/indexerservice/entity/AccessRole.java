package com.acme.indexerservice.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "ACCESS_ROLE")
@IdClass(AccessRoleId.class)
public class AccessRole implements Serializable {

    @Id
    @Column(name = "USER_ID", length = 200)
    private Integer userId;

    @Id
    @Column(name = "DOC_ID")
    private Integer docId;

    @Id
    @Column(name = "FOLDER_ID")
    private Integer folderId;


}
