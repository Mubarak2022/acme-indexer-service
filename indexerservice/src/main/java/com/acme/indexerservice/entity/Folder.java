package com.acme.indexerservice.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "FOLDER", uniqueConstraints = {@UniqueConstraint(name = "uniqueFolderNameAndParentId", columnNames = {"FOLDER_NAME", "PARENT_FOLDER"})})
public class Folder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FOLDER_ID")
    private Integer folderId;

    @Column(name = "FOLDER_NAME", length = 200)
    private String folderName;

    @ManyToOne
    @JoinColumn(name = "PARENT_FOLDER")
    private Folder parentFolder;

    @Column(name = "IS_DELETED")
    private Boolean isDeleted;

    @Column(name = "CREATED_DATE")
    private ZonedDateTime createdDate;

    @Column(name = "MODIFIED_DATE")
    private ZonedDateTime modifiedDate;

    @Column(name = "DELETED_DATE")
    private ZonedDateTime deletedDate;

    @NotNull
    @Column(name = "CREATED_BY_USER")
    private Integer createdByUser;

    @Column(name = "DELETED_BY_USER")
    private Integer deletedByUser;


}
