package com.acme.indexerservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccessRoleId implements Serializable {

    private Integer userId;
    private Integer docId;
    private Integer folderId;


}
